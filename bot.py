#!/usr/bin/env python3

import irc.bot

class BotFreemind(irc.bot.SingleServerIRCBot):

    server  = "localhost"
    port    = 6667
    nickname = "MitNick"

    def __init__(self):
        irc.bot.SingleServerIRCBot.__init__(self, [(self.server, self.port)], self.nickname, self.nickname)

    def on_welcome(self, serv, ev):
        serv.join("#Bot")




if __name__ == "__main__":
    BotFreemind().start()
