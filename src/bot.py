#!/usr/bin/env python3

import irc.bot
import re

# ev.source.nick = recupere l'evenement
# ev.target      = cible de l'evenement


class BotFreemind(irc.bot.SingleServerIRCBot):

    server   = "irc.freemind.ovh" # Fill the sever's name : ex : irc.freemind.org
    port     = 6667 # The irc port ex : 6667
    nickname = "Alibot" # Tha name of the bot
    master   = "Alibaba" # The master of the bot

    def __init__(self):
        irc.bot.SingleServerIRCBot.__init__(self, [(self.server, self.port)], self.nickname, self.nickname)

    def on_welcome(self, serv, ev):
        serv.join("#Bot")

    def on_pubmsg(self, serv, ev):
      canal = ev.target
      message = ev.arguments
      source = ev.source.nick
      #if (source is "Alibaba"):
      #  print("Hello")
      serv.privmsg(canal, message[0])
      serv.disconnect("Je m'en vais.")

if __name__ == "__main__":
    BotFreemind().start()
